/*
2. Create a simple server and the following routes with their corresponding HTTP methods and responses:
- If the url is http://localhost:4000/, send a response Welcome to Booking System
- If the url is http://localhost:4000/profile, send a response Welcome to your profile!
- If the url is http://localhost:4000/courses, send a response Here’s our courses available
- If the url is http://localhost:4000/addcourse, send a response Add a course to our resources
- If the url is http://localhost:4000/updatecourse, send a response Update a course to our resources
- If the url is http://localhost:4000/archivecourses, send a response Archive courses to our resources
3. Test all the endpoints in Postman.
*/

const http = require("http");

const port = 4000;

const server = http.createServer((request, response) => {
	if(request.url == "/" && request.method == "GET"){
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Welcome to Booking System.");
	}
	if(request.url == "/profile" && request.method == "GET"){
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Welcome to your profile.");
	}
	if(request.url == "/courses" && request.method == "GET"){
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Here's our courses available.");
	}
	if(request.url == "/addCourse" && request.method == "POST"){
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Add a course to our resources.");
	}
	if(request.url == "/updateCourse" && request.method == "PUT"){
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Update a course to our resources.");
	}
	if(request.url == "/archiveCourse" && request.method == "DELETE"){
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Archive courses to our resources");
	}
});

server.listen(port);

console.log(`Server is running at localhost: ${port}`);